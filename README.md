# IronWorks.Kaitai

## Project Description

__IronWorks.Kaitai__ is a collection of [Kaitai Struct][1] `.ksy` files for
parsing and exploring the various files and data structures employed in FINAL
FANTASY XIV: A Realm Reborn.

These can be used with the Kaitai Structure Viewer (either as a local console
application or using the [web version here][2]), or compiled into a parser in
one of many popular languages. See the [Kaitai Struct][1] webpage for more
details.

[1]: https://kaitai.io/
[2]: https://ide.kaitai.io/

## License

__IronWorks.Kaitai__ is released under the MIT License ([SPDX MIT][3]). A
[markdown version][4] of the license is provided in the repository.

[3]:https://spdx.org/licenses/MIT.html
[4]:LICENSE.md
