# A thin wrapper around sq_pack_index.ksy

meta:
  id: sq_pack_index_file
  title: 'Square-Enix SqPack .index file'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  file-extension: .index
  license: CC0-1.0

  imports:
    - sq_pack_index

doc: |
  Represents an SqPack .index file.

seq:
  - id: index
    type: sq_pack_index(1)
