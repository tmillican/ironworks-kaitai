# This grammar encapsulates the differences between .index and .index2 files
# with a single parameter (see: index_type).
#
# To parse .index(2) files directly (ex. from ksv), you sould use either:
#
#   sq_pack_index.ksy
#   sq_pack_index2.ksy

meta:
  id: sq_pack_index
  endian: le
  title: 'Square-Enix SqPack index structure'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  license: CC0-1.0

  imports:
    - sq_pack_file_header
doc: |
  The root structure of an SqPack .index file.

params:
  - id: index_type
    type: u1
    doc: |
      Indicates the index version. 1 is for .index, 2 is for .index2.

      Folders are present in .index files, but not in .index2 files. As well,
      the file descriptors of .index2 files are shorter, ommitting the folder
      name hash and padding field.
seq:
  - id: file_header
    type: sq_pack_file_header(false)
    doc: |
      The file header common to all SqPack files (.dat, .index, etc.)
  - id: segment_header
    type: index_segment_header
    doc: |
      The file header common to all SqPack files (.dat, .index, etc.)

instances:
  file_segment:
    type: file_table
    pos: segment_header.segment_table[0].offset
    size: segment_header.segment_table[0].size
    # I would think that this is mandatory for an index file, but you just never
    # know with SE...
    if: segment_header.segment_table[0].size > 0
    doc: |
      The segment containing the file table of the index.
  unknown_segment1:
    pos: segment_header.segment_table[1].offset
    size: segment_header.segment_table[1].size
    if: segment_header.segment_table[1].size > 0
    doc: |
      The semantics of this segment are unknown.

      When present, it seems to always be 256 (0x100) bytes long, and
      immediately follows the file table segment. The first 16 bytes
      are always:

        FF FF FF FF FF FF FF FF 00 00 00 00 FF FF FF FF

      The remaining bytes are always null (0x00).
  unknown_segment2:
    type: segment2_data
    pos: segment_header.segment_table[2].offset
    size: segment_header.segment_table[2].size
    if: segment_header.segment_table[2].size > 0
    doc: |
      The semantics of this segment are unknown.
  folder_segment:
    type: folder_table
    pos: segment_header.segment_table[3].offset
    # size: segment_header.segment_table[3].size
    if: (index_type == 1) and (segment_header.segment_table[3].size > 0)
    doc: |
      The segment containing the folder table of the index.

types:
  index_segment_header:
    doc: |
      Describes the segments present in an .index file.
    seq:
      - id: size
        type: s4
        doc: |
          The size of the segment header, in bytes.
      - id: segment_table
        type: segment_descriptor(_index == 0)
        repeat: expr
        repeat-expr: 4
        doc: |
          Only 4 segment descriptors are known to exist. Segment 0 is files,
          segment 3 is folders. Any data beyond the first 4 segments is regarded
          as padding.
      - id: segment_table_padding
        # The size field and footer occupy 4 and 64 bytes respectively. Segment
        # descriptors are typically 72 bytes, plus 4 extra for the first
        # descriptor. So:
        size: size - 64 - 4 - (72 * 4 + 4)
      - id: footer
        type: sq_pack_section_footer
  # [end segment_header]

  segment_descriptor:
    doc: |
      Describes a single segment (ex. a file descriptor segment) of an
      .index file.
    params:
      # used to determine if 4 extra bytes should be added to the padding
      - id: is_first
        type: bool
    instances:
      padding_size:
        value: '40 + (is_first ? 4 : 0)'
    seq:
      - id: unknown
        type: u4
        doc: |
          For segment 0 (files), may be the .dat file count
      - id: offset
        type: u4
        doc: |
          Offset to the segment data
      - id: size
        type: s4
        doc: |
          The size of the segment data, in bytes.
      - id: sha
        size: 20
        doc: |
          The SHA-1 hash of the segment data.
      - id: padding
        size: padding_size
        doc: |
          The first index segment descriptor in the index segment table has 4
          extra bytes of padding.
  # [end segment_descriptor]

  file_table:
    doc: |
      A table of file descriptors.
    seq:
      - id: entries
        type: descriptor
        repeat: eos

    types:
      descriptor:
        doc: |
          Identifies a file in an archive.
        seq:
          - id: name_hash
            type: u4
            doc: |
              A hash of the file name.
          - id: folder_hash
            type: u4
            if: _root.index_type == 1
            doc: |
              A hash of the parent folder's name.
          - id: packed_location
            type: u4
            doc: |
              A packed field containing the location of the file data. This is a
              combination of the offset and .dat volume number.
          - id: padding
            size: 4
            if: _root.index_type == 1
            doc: |
              Semantics unknown.

              So far as is known, these bytes are always null padding in .index
              files.

              There is no obvious connection between this field and folders, but
              this field is ommitted in .index2 files, along with the folder name
              hash.
        instances:
          data_offset:
            value: (packed_location & 0xFFFFFFF8) << 3
            doc: |
              The offset to the file data relative to the beginning of the
              containing .dat file.
          data_volume_id:
            value: (packed_location & 0x7) >> 1
            doc: |
              The offset to the file data relative to the beginning of the
              containing .dat file.
        # [end descriptor]
  # [end file_table]

  folder_table:
    doc: |
      A table of folder descriptors.
    seq:
      - id: entries
        type: descriptor
        # While it might be cleaner to do this by setting the size of the
        # folder_table at the sq_pack_index level and using "repeat: eos", this
        # breaks parsing of the directories file table because the "files"
        # instance can't escape the folder_table's substream.
        repeat: expr
        repeat-expr: _root.segment_header.segment_table[3].size / 16

    types:
      descriptor:
        doc: |
          Identifies a folder in an archive.
        seq:
          - id: name_hash
            type: u4
            doc: |
              A hash of the folder name.
          - id: file_table_offset
            type: u4
            doc: |
              An offset to this folder's file table, in bytes, relative to the
              beginning of the .index file.
          - id: file_table_size
            type: s4
            doc: |
              The size of this folder's file table, in bytes.
          - id: padding
            size: 4
            doc: |
              Semantics unknown. So far as is known, these bytes are always null
              padding in .index folders.
        instances:
          files:
            pos: file_table_offset
            size: file_table_size
            type: file_table
      # [end descriptor]
  # [end folder_table]

  segment2_data:
    doc: |
      The semantics of this data is unknown, but the syntax suggests a table of
      16-byte records, each of which consists of 4 x 32-bit values.

      Guess: probably some kind of auxillary file table. Generally speaking, the
      more .dat files are in the archive, the larger this segment is.

      My suspicion is that this used to index into .dat4 and beyond. For one,
      this segment is (relatively) gigantic for 040000.wind32.index (~6MB, ~500k
      entries). Moreover, only 2 bits are used describe the volume number in the
      file descriptors of the file table segment. Obviously that only works for
      .dat[0-3]. There must be some other mechanism for addressing .dat4 and
      (hypothetically) beyond.
    seq:
      - id: entries
        type: record
        repeat: eos
    types:
      record:
        doc: |
          Semantics unknown.

          Guess: probably some kind of file descriptor.

          All of the values in the record fields seem to be monotonically
          increasing down the table. This follows the pattern of the file and
          directory segments.
        seq:
          - id: unknown1
            type: u4
            doc: |
              Semantics unknown.

              Guess: 1-indexed .dat identifier?

              Worth noting is that the value in this field never exceeds the
              number of .dat files in the archive, as far as I can tell (using a
              Mk. 1 eyeball, mind you).
          - id: unknown2
            type: u4
            doc: |
              Semantics unknown.

              Guess: This looks like an offset into a file, possibly/probably
              shifted by 7 bits. The most significant byte never seems to be
              non-zero, which is reminiscent of the way file descriptors work.
          - id: unknown3
            type: u4
            doc: |
              Semantics unknown.

              Guess: hard to say. This field usually doesn't change much from
              record to record, but when you get close to the end of table, it
              often grows very quickly. It always seems to stay in the range of
              a 2-byte integer, though.
          - id: unknown4
            type: u4
            doc: |
              Semantics unknown. Seems to always be null (0x00) bytes.

              This behavior is identical to what we see in the 16-byte (long)
              file descriptor.
      # [end record]
  # [end segment2_data]
# [end sq_pack_index]
