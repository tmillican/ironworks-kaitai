meta:
  id: sq_pack_data_block
  endian: le
  title: 'Square-Enix SqPack stored file structure'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  license: CC0-1.0

doc: |
  Represents the SqPack data block format.

params:
  - id: block_size
    type: u4
    doc: |
      The total size of the block, in bytes, including the header
      data and padding.
seq:
  - id: metadata
    type: block_metadata
    doc: |
      Metadata describing the block.
  - id: data
    size: metadata.data_size
    doc: |
      The block payload.
  - id: padding
    size: block_size - metadata.data_size - metadata.header_size
    doc: |
      Padding bytes that follow the block data if the data is smaller than
      the block size.
instances:
  is_compressed:
    value: metadata.compressed_size != 32000
    doc: |
      Indicates whether the block is compressed.

types:
  block_metadata:
    seq:
      - id: header_size
        type: s4
        doc: |
          The block header size, in bytes. Maybe always 0x10?
      - id: unknown
        size: 4
        doc: |
          Maybe always null (0x00) bytes? Possibly padding.
      - id: compressed_size
        type: s4
        doc: |
          The size of the block data, in bytes. If this is 32000 (0x7D00), the
          block is not compressed. Otherwise, the block data is a DEFLATE
          (RFC-1951) stream.
      - id: uncompressed_size
        type: s4
        doc: |
          The size of the uncompressed block data, in bytes. Never exceeds 16kB?
      - id: header_padding
        size: header_size - 16
        doc: |
          Header padding. Probably an empty set of bytes. It appears that the
          header size field is always 0x10 (16), making the padding
          non-existent.  Alternatively, it may be that the 'unknown' field of
          the block header is not a fixed-width unknown field, but rather a
          variable-width padding field.
    instances:
      data_size:
          value: "(_parent.is_compressed ? compressed_size : uncompressed_size)"
