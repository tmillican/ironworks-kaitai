meta:
  id: sq_pack_section_footer
  title: 'Square-Enix SqPack file section footer structure'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  license: CC0-1.0

doc: |
  The SqPack footer structure follows several file sections in multiple SqPack
  file types. Ex: the index segment header.

seq:
  - id: sha1
    size: 20
    doc: |
      SHA-1 hash of the preceeding section.

      It is likely that this hash was intended for ECC, rather than
      detecting client modification. Either way, the FFXIV client presently
      seems to ignore this value altogether (there are hash mismatches in
      several of the official game files).
  - id: padding
    size: 44
    doc: |
      So far as is known, the remainder of the footer is always null
      padding.
