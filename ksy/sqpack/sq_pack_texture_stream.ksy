meta:
  id: sq_pack_texture_stream
  endian: le
  title: 'Square-Enix SqPack stored file structure'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  license: CC0-1.0

  imports:
    - sq_pack_data_block

doc: |
    Represents a stream used to store texture data.

    Broadly speaking, the stream is composed of an uncompressed file header
    (always 0x80 bytes?) followed by a series of 'frames'. Each frame
    contains a series of DEFLATE compressed blocks. The uncompressed size of
    the frames seems to always be a power of 2, and the frames are sorted in
    descending order of uncompressed size. Consecutive frames may have the
    same uncompressed size, at least at the very end of the frame stream.

params:
  - id: stream_offset
    type: s8
    doc: |
      The absolute offset of the texture stream in its data volume file.
  - id: total_header_size
    type: s4
    doc: |
      The combined size of all data stream headers (common + type-specific).
  - id: common_header_size
    type: s4
    doc: |
      The size of the common (non-type-specific) data stream header.
seq:
  - id: frame_count
    type: u4
    doc: |
      The number of frames in this stream.
  - id: frames
    type: texture_frame
    repeat: expr
    repeat-expr: frame_count
    doc: |
      The set of frames comprising this texture data stream.
instances:
  original_file_header:
    pos: stream_offset + total_header_size
    size: frames[0].frame_offset
    doc: |
      The original, pre-packed file header. Maybe always 0x80 bytes in length?

      This must be prepended to the uncompressed texture frames to
      reconstitute the original file.

types:
  texture_frame:
    seq:
      - id: frame_offset
        type: u4
        doc: |
          The starting offset of this frame, in bytes, relative to the end
          of the stream headers.
      - id: frame_size
        type: u4
        doc: |
          The total size of this frame in bytes. I.e. this should be the sum
          of all the raw block sizes (block header and padding included) in
          this frame.
      - id: uncompressed_size
        type: u4
        doc: |
          The total uncompressed size this frame. I.e. this should be the sum
          of all the blocks' uncompressed sizes.
      - id: first_block_id
        type: u4
        doc: |
          The block number of the first block in this frame.
      - id: block_count
        type: u4
        doc: |
          The number of blocks in this frame.
    instances:
      block_sizes:
        pos: _parent.stream_offset + _parent.common_header_size + (_parent.frame_count * 20) + 4 + (2 * first_block_id)
        type: u2
        repeat: expr
        repeat-expr: block_count
        doc: |
          The table of block sizes for this frame.
      blocks:
        pos: _parent.stream_offset + _parent.total_header_size + frame_offset
        type: sq_pack_data_block(block_sizes[_index])
        repeat: expr
        repeat-expr: block_count
        doc: |
          The data blocks belonging to this frame.
  # [ end subtype: texture_frame ]
