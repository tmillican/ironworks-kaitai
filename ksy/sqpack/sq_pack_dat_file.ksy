# A thin wrapper around sq_pack_dat.ksy
#
# If parsing fails due to bad magic bytes, and you are sure this is a valid
# SqPack .dat file, try sq_pack_mangled_dat_file.ksy instead.

meta:
  id: sq_pack_dat_file
  title: 'Square-Enix SqPack .datN file'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  file-extension:
    - .dat0
    - .dat1
    - .dat2
    - .dat3
    - .dat4
  license: CC0-1.0

  imports:
    - sq_pack_dat

doc: |
  Represents the metadata of an SqPack .dat[0...N] file.

seq:
  - id: dat
    type: sq_pack_dat(false)
