meta:
  id: sq_pack_model_stream
  endian: le
  title: 'Square-Enix SqPack stored file structure'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  license: CC0-1.0

  imports:
    - sq_pack_data_block


params:
  - id: stream_offset
    type: s8
    doc: |
      The absolute offset of the model stream in its data volume file.
  - id: total_header_size
    type: s4
    doc: |
      The combined size of all data stream headers (common + type-specific).
  - id: common_header_size
    type: s4
    doc: |
      The size of the common (non-type-specific) data stream header.
doc: |
  Represents a stream used to store model data.
seq:
  - id: unknown1
    type: u2
    doc: |
      Uknown. Guess: there seems to always be a gap of 3 null entries in the
      block tables following the first 5 entries. This value always seems to
      be 5. This might describe the length of the first run of blocks. I
      have no idea what makes those blocks special, though.
  - id: unknown2
    type: u2
    doc: |
      Uknown. Guess: This seems to always have the same value as the header
      region size described in the common stream header. It's hard to be
      sure, though, since both values seem to always be 256.
  - id: frame_metadata
    type: model_frame_metadata
    doc: |
      A set of tables describing the frames in this stream. Probably only
      meaningful to the parser.
  - id: unknown3
    size: 8
    doc: |
      Your guess is as good as mine.
  - id: block_sizes
    type: u2
    repeat: expr
    repeat-expr: 23
    doc: |
      The master block size table. Each frame references this table by block
      id to locate its blocks.
instances:
  frames:
    type: model_frame(_index)
    repeat: expr
    repeat-expr: 11
    doc: |
      The set of frames in this model stream.

types:
  model_frame_metadata:
    seq:
      - id: uncompressed_sizes
        type: u4
        repeat: expr
        repeat-expr: 11
        doc: |
          A table of the uncompressed sizes of the frames in this stream. These
          are typically a bit larger than the sum of the uncompressed sizes in
          the block headers of the blocks for this frame.
      - id: sizes
        type: u4
        repeat: expr
        repeat-expr: 11
        doc: |
          A table of the total sizes of the frames in this stream.
      - id: offsets
        type: u4
        repeat: expr
        repeat-expr: 11
        doc: |
          A table of the starting offsets of the frames in this stream.
      - id: starting_block_ids
        type: u2
        repeat: expr
        repeat-expr: 11
        doc: |
          A table of the starting block id of the frames in this stream.
      - id: block_counts
        type: u2
        repeat: expr
        repeat-expr: 11
        doc: |
          A table of the block counts of the frames in this stream.
  # [ end type: model_frame_metadata ]

  model_frame:
    params:
      - id: frame_id
        type: s4
        doc: |
          The frame number. Used as an index into the various frame data tables.
    instances:
      blocks:
        pos: _parent.stream_offset + _parent.total_header_size + _parent.frame_metadata.offsets[frame_id]
        type: sq_pack_data_block(_parent.block_sizes[_parent.frame_metadata.starting_block_ids[frame_id] + _index])
        repeat: expr
        repeat-expr: _parent.frame_metadata.block_counts[frame_id]
        doc: |
          The blocks belonging to this frame.
  # [ end type: model_frame ]
