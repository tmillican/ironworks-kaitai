# A thin wrapper around sq_pack_dat.ksy

meta:
  id: sq_pack_mangled_dat_file
  title: 'Square-Enix SqPack mangled .datN file'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  file-extension:
    - .dat0
    - .dat1
    - .dat2
    - .dat3
    - .dat4
  license: CC0-1.0

  imports:
    - sq_pack_dat

doc: |
  Represents an SqPack .dat[0...N] file with a mangled header.

  Presently known offenders are:

    ex1/030100.win32.dat1
    ffxiv/020000.win32.dat2
    ffxiv/030000.win32.dat1
    ffxiv/040000.win32.dat2
    ffxiv/040000.win32.dat3

seq:
  - id: dat
    type: sq_pack_dat(true)
