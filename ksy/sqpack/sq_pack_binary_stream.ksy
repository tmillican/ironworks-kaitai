meta:
  id: sq_pack_binary_stream
  endian: le
  title: 'Square-Enix SqPack stored file structure'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  license: CC0-1.0

  imports:
    - sq_pack_data_block

doc: |
    Represents a stream used to store arbitrary binary data.

params:
  - id: stream_offset
    type: s8
    doc: |
      The absolute offset of the binary stream in its data volume file.
  - id: total_header_size
    type: s4
    doc: |
      The combined size of all data stream headers (common + type-specific).
  - id: common_header_size
    type: s4
    doc: |
      The size of the common (non-type-specific) data stream header.
seq:
  - id: block_count
    type: u4
    doc: |
      The number of blocks in the binary block stream.
  - id: block_descriptors
    type: binary_block_descriptor
    repeat: expr
    repeat-expr: block_count
    doc: |
      A sequence of block descriptors (offset, size, etc.) describing the
      blocks in this binary stream.
  - id: header_padding
    size: total_header_size - common_header_size - 4 - block_count * 8
    doc: |
      Padding bytes that may follow the binary stream header if the combined
      size of the common stream header and the binary stream header is less
      than the header region size, as described by the common stream header.
instances:
  blocks:
    type: binary_block(_index)
    repeat: expr
    repeat-expr: block_count
    doc: |
      A wrapper around a common block. This is a bit of a conceit to work
      around a bug(?) in Kaitai Struct. See:

      https://github.com/kaitai-io/kaitai_struct/issues/436

types:
  binary_block_descriptor:
    doc: |
      A descriptor for a block in a binary file stream.
    seq:
      - id: offset
        type: u4
        doc: |
          Offset to the block, in bytes, relative to the beginning of the
          block data section.
      - id: size
        type: u2
        doc: |
          The size of the block, in bytes.
      - id: uncompressed_size
        type: u2
        doc: |
          The uncompressed size of the block, in bytes.
  # [ end subtype: binary_block_descriptor ]

  binary_block:
    doc: |
      A wrapper around a common block. This is a bit of a conceit to work
      around a bug(?) in Kaitai Struct. See:

      https://github.com/kaitai-io/kaitai_struct/issues/436
    params:
      - id: block_id
        type: s4
    instances:
      block:
        pos: _parent.stream_offset + _parent.total_header_size + _parent.block_descriptors[block_id].offset
        type: sq_pack_data_block(_parent.block_descriptors[block_id].size)
        doc: |
          A common data block.
  # [ end subtype: binary_block ]
