meta:
  id: sq_pack_stored_file_stream
  endian: le
  title: 'Square-Enix SqPack stored file structure'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  license: CC0-1.0

  imports:
    - sq_pack_binary_stream
    - sq_pack_texture_stream
    - sq_pack_model_stream

doc: |
  A stored file within an SqPack .dat file's data segment.

params:
  - id: stream_offset
    type: s8
    doc: |
      The absolute offset of the stored file stream in its data volume file.
seq:
  - id: total_header_size
    type: s4
    doc: |
      The size of the entire data stream header region. Includes the common data
      stream header, type-specific headers, and padding.

      Essentially, this indicates where the block data begins, except that for
      texture files, the original file header is wedged inbetween the end of the
      header and the start of the block data.
  - id: stream_type
    type: u4
    enum: content_type
    doc: |
      The content type of the stored data stream.
  - id: uncompressed_size
    type: s4
    doc: |
      The uncompressed size of the file.

      For texture streams, this value may be larger than the sum of all blocks'
      uncompressed sizes.  The difference should be equal to the size of the
      original file header. The original file header is stored in an
      uncompressed region immediately preceding the first frame's block stream.

      For model streams, this total is often larger than than the sum of all blocks'
      uncompressed sizes. Reasons presently unknown. Maybe rounding to a multiple
      of a power of two?
  - id: unknown1
    type: u4
    doc: |
      Seems to be the value of unknown2, rounded up by some unknown criterion.

      This somewhat jives with the unknown2 description given by ioncannon since
      a similar scheme is often used with block buffers, particularly for model
      data streams but the sizes just don't match the block stream in any
      comprehensible way (too small, sometimes by multiple orders of magnitude)
  - id: unknown2
    type: s4
    doc-ref: "http://ffxivexplorer.fragmenterworks.com/research/sqpack%20dat%20files.txt"
    doc: |
      Seems to be the pre-rounded value of unknown1. Meaning unknown.

      ioncannon's notes call this the 'block buffer size' with the description:
      'buffer size need[ed] to read largest block'. I have no idea what this
      means, as most block streams contain a block far larger than this
      value. From my conversations with another modder, The SqPack format has
      gone through a number of subtle changes over the years, so this might be
      an ancient artifact.
  - id: data_stream
    type:
      switch-on: stream_type
      cases:
        # 20 = common header size
        "content_type::binary": sq_pack_binary_stream(stream_offset, total_header_size, 20)
        "content_type::model": sq_pack_model_stream(stream_offset, total_header_size, 20)
        "content_type::texture": sq_pack_texture_stream(stream_offset, total_header_size, 20)
    doc: |
      The stored file's data stream

enums:
  content_type:
    1: empty
    2: binary
    3: model
    4: texture
