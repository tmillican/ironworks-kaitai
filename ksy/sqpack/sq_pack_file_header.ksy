meta:
  id: sq_pack_file_header
  endian: le
  title: 'Square-Enix SqPack file header structure'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  license: CC0-1.0

  imports:
    - sq_pack_section_footer

doc: |
    Common file header for all SqPack file types (SQDB, index, data).

params:
  - id: is_mangled
    type: bool
    doc: |
      Indicates whether the header has been "mangled". Setting this option to
      true will ignore a bad signature and assume the length of the header is
      the standard 0x400 (1024) bytes.

      Some official SqPack files (ex: 020000.win32.dat2) have mangled headers.
      The signature field has been overwritten with "0x80" followed by 11 null
      (0x00) bytes, and the size field is overwritten with 0x0F. The SHA-1 of
      the header is also incorrect, though this is also sometimes the case with
      otherwise normal SE headers.

seq:
  - id: valid_magic
    size: 12
    if: not is_mangled
    contents: [ "SqPack", 0, 0, 0, 0, 0, 0 ]
    doc: |
      Validated magic bytes. That is: ASCII "SqPack" followed by 6 null (0x00)
      bytes.

  - id: maybe_magic
    size: 12
    if: is_mangled
    doc: |
      The raw, unvalidatd magic bytes. May or may not be valid magic bytes.

  - id: size
    type: s4
    doc: |
      The size of the header, in bytes, as reported by the header.

      Please note that if IsMangled is true, the actual header size is forced to
      be 0x400 (1024) bytes, regardless of this value.

  - id: unknown1
    size: 4
    doc: |
      Semantics unknown. Value seems to always match the "unknown" field of
      the segment table.

  - id: type
    type: u4
    enum: sq_pack_file_type
    doc: |
      The type of the SqPack file.

    # The unknown2 is (or contains) the "slack" field for the variable-length
    # header. We compute unknown2 according to is_mangled and the declared
    # header.size. See unknown2_size for details.
  - id: unknown2
    size: unknown2_size
    doc: |
      Semantics unknown. This is probably one or more fixed-size data fields
      followed by variable padding, but it is unclear where the data ends
      and the padding begins.

  - id: footer
    type: sq_pack_section_footer

instances:
  unknown2_size:
    # If is_mangled == true, we ignore the size field value and assume a header
    # size of 0x400, emulating the behavior of the FFXIV client.
    #
    # The signature is 12 bytes. The size, unknown1, and type fields are 4 bytes
    # each. The footer is 64 bytes. The remainder is attributed to the unknown2
    # field.
    value: '(is_mangled ? 0x400 : size) - 12 - (3 * 4) - 64'

enums:
  sq_pack_file_type:
    0: sqdb
    1: data
    2: index
