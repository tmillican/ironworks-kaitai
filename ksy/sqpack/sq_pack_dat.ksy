meta:
  id: sq_pack_dat
  endian: le
  title: 'Square-Enix SqPack .dat core structure'
  application: 'FINAL FANTASY XIV: A Realm Reborn'
  license: CC0-1.0

  imports:
    - sq_pack_file_header

doc: |
  The metadata of an SqPack .datN file.

params:
  - id: is_mangled
    type: bool
    doc: |
      Indicates whether this .dat file is expected to have a mangled header.
      This is the case for some of the official FFXIV files.

      Presently known offenders are:

        ex1/030100.win32.dat1
        ffxiv/020000.win32.dat2
        ffxiv/030000.win32.dat1
        ffxiv/040000.win32.dat2
        ffxiv/040000.win32.dat3

      This disables magic bytes verification, and will treat the header as
      though it has a length of 0x400 (1024) bytes, regardless of the header's
      size field. This emulates the behavior of the FFXIV client, so it should
      be safe to use with any of the official game files, but use at your own
      risk!

seq:
  - id: file_header
    type: sq_pack_file_header(is_mangled)
  - id: data_header
    type: data_section_header

types:
  data_section_header:
    doc: |
      Data file header with .dat-specific metadata.
    seq:
      - id: size
        type: s4
        doc: |
          The size of the data file header, in bytes.
      - id: unknown1
        size: 4
        doc: |
          Unknown semantics. Maybe always null padding.
      - id: unknown2
        size: 4
        doc: |
          Unknown semantics. Maybe always 0x10?
      - id: raw_data_size
        type: u4
        doc: |
          The size of the data segment (everything after the data file header),
          right-shifted by 7.

          Speculation: the use of a 7-bit shift here suggests a signed value
          that is always expected to be positive (makes sense for a size field
          in a format that may need to support hardware and languages that don't
          do unsigned 32-bit integers). But why shift at all? Just to enforce
          128-byte alignment? I can't help wondering if the raw field is a
          packed value with something occupying the upper byte. So far, it's
          always been 0, though.
      - id: spanned_dat
        type: u4
        doc-ref: |
          http://ffxivexplorer.fragmenterworks.com/research/sqpack%20dat%20files.txt
        doc: |
          Quoting ioncannon here: "0x010: Spanned DAT, Int32; 0x01 = .dat0, 0x02
          = .dat1 or .dat2, etc"

          I have no idea what pattern he's trying to describe. I think maybe
          this is just a boolean flag for whether this is a multi-volume
          archive.
      - id: unknown3
        size: 4
        doc: |
          Unknown semantics. Maybe always null padding.
      - id: max_file_size
        type: u4
        doc: |
          The maximum size of a .dat file in this volume sequence? Maybe? I'm
          guessing it's actually the max buffer size needed to accomodate this
          file if you were crazy enough to load the whole thing into memory at
          once.
      - id: unknown4
        size: 4
        doc: |
          Unknown semantics. Maybe always null padding.
      - id: data_sha1
        size: 20
        doc: |
          The SHA-1 hash of the data segment.

          The data segment is comprised of all of the bytes following the data
          file header to EOF.
      - id: padding
        # 8 x 4-byte fields,20 bytes for SHA-1, 64 bytes footer. What remains
        # is the padding.
        size: size - 8 * 4 - 20 - 64
        doc: |
          Padding bytes.
      - id: footer
        type: sq_pack_section_footer

    instances:
      data_size:
        value: raw_data_size << 7
  # [end data_section_header]
# [end sq_pack_dat_core]
